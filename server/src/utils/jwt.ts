import jwt, { SignOptions } from 'jsonwebtoken';
import environment from '../config/environment';

export interface JwtPayload {
	sub?: string;
	name?: string;
    admin?: boolean;
    email?: string;
};

export default class JWTUtiles {
    static generateAccessToken(payload: JwtPayload, options: SignOptions ={}) {
        const { expiresIn = '1d' } = options;
        return jwt.sign(payload, environment.jwtTokenSecret, { expiresIn });
    }

    static generateRefreshToken(payload: JwtPayload) {
        return jwt.sign(payload, environment.jwtRefreshTokenSecret);
    }

    static verifyAccessToken(accessToken: string) {
        return jwt.verify(accessToken, environment.jwtTokenSecret);
    }

    static verifyRefreshToken(accessToken: string) {
        return jwt.verify(accessToken, environment.jwtRefreshTokenSecret);
    }
}

export default {
    port: process.env.PORT? parseInt(process.env.PORT) : 5000,
    nodeEnv: process.env.NODE_ENV || 'production',
    jwtTokenSecret: process.env.JWT_ACCECC_TOKEN_SECRET || 'mock_token_secret',
    jwtRefreshTokenSecret: process.env.JWT_REFRESH_TOKEN_SECRET || 'mock-refresh-token-secret'
}
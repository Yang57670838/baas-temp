const path = require('path');
const {
    BundleAnalyzerPlugin
} = require('webpack-bundle-analyzer');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');

module.exports = {
    webpack: function (config, env) {

        // add plugin
        if (!config.plugins) {
            config.plugins = [];
        }
        config.plugins.push(new BundleAnalyzerPlugin({
            // use value from environment var STATS or 'disabled'
            analyzerMode: process.env.STATS || 'disabled',
        }));
        config.plugins.push(new ModuleFederationPlugin(
            {
              name: 'form',
              remotes: {
                contactus: 'contactus@http://localhost:3006/contactus.js'
              }
            }
          ));
        config.resolve = {
            ...config.resolve,
            alias: {
                ...config.resolve.alias,
                components: path.resolve(__dirname, 'src/components'),
                constants: path.resolve(__dirname, 'src/constants'),
                mocks: path.resolve(__dirname, 'src/mocks'),
                store: path.resolve(__dirname, 'src/store'),
                types: path.resolve(__dirname, 'src/types'),
                utility: path.resolve(__dirname, 'src/utility'),
                contexts: path.resolve(__dirname, 'src/contexts'),
                assets: path.resolve(__dirname, 'src/assets'),
                hooks: path.resolve(__dirname, 'src/hooks'),
                locales: path.resolve(__dirname, 'src/locales')
            },
        };
        return config;
    },
    jest: function (config) {
        // ...add your jest config customisation...
        return config;
    },
    devServer: function (configFunction) {
        return function (proxy, allowedHost) {
            const config = configFunction(proxy, allowedHost);

            config.proxy = {
                ...config.proxy,
                '/bff': {
                    // /api requrest send to local nodeJS app for test purpose, at port 5000
                    target: 'http://localhost:5000',
                    secure: false,
                    logLevel: 'debug',
                    pathRewrite: {
                        '^/bff': ''
                    },
                }
            }
            return config;
        };
    },
    paths: function (paths, env) {
        return paths;
    },
}
import { Theme, ThemeOptions } from "@material-ui/core/styles/createTheme";
import { Typography } from "@material-ui/core/styles/createTypography";

interface ITypography extends Typography {
    tab?: {
        textTransform: string;
        fontweight: number;
        fontSize: string;
    };
    headerButton?: {
        fontSize: string;
        textTransform: string;
        color: string;
    };
}

export interface ITheme extends Theme {
    typography: ITypography;
}

export interface IThemeOptions extends ThemeOptions {
    typography?: ITypography;
}

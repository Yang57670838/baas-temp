const reader = require('xlsx');
const path = require('path');

export const readCsv = () => {
    console.log(__dirname);
    const dirPath = path.join(__dirname, '../mock/purchase.xlsx');
    console.log(dirPath);
    const file = reader.readFile(dirPath);
    let data: any = []

    const sheets = file.SheetNames

    // for (let i = 0; i < sheets.length; i++) {
    //     const temp = reader.utils.sheet_to_json(
    //         file.Sheets[file.SheetNames[i]])

    //     temp.forEach((res: any) => {
    //         data.push(res)
    //     })
    // }

    const temp = reader.utils.sheet_to_json(
        file.Sheets[file.SheetNames[2]])

    temp.forEach((res: any) => {
        data.push(res)
    })
    console.log(data)
}
import { useEffect, useRef } from "react";

export function useDebounce(callback: (arg?: any) => void, wait: number) {
    // track timeout handle between calls
    const timeout = useRef<ReturnType<typeof setTimeout>>();

    function cleanup() {
        if (timeout.current) {
            clearTimeout(timeout.current);
        }
    }

    // make sure our timeout gets cleared if
    // our consuming component gets unmounted
    useEffect(() => cleanup, []);

    return function debouncedCallback(arg?: any) {
        // clear debounce timer
        cleanup();

        // start waiting again
        timeout.current = setTimeout(() => {
            callback(arg);
        }, wait);
    };
}

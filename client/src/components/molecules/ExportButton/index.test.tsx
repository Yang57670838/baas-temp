import { screen, render } from '@testing-library/react';
import userEvent, { TargetElement } from "@testing-library/user-event";
import renderer from "react-test-renderer";
import ExportButton from './index';
import React from "react";

const mockPayload = [
    { firstname: "Ahmed", lastname: "Tomi", email: "ah@smthing.co.com" },
    { firstname: "Raed", lastname: "Labes", email: "rl@smthing.co.com" },
    { firstname: "Yezzi", lastname: "Min l3b", email: "ymin@cocococo.com" },
];

describe('ExportButton component snapshot', () => {
    test('renders correctly', () => {
        const tree = renderer.create(<ExportButton payload={mockPayload} />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});

describe('ExportButton component', () => {
    test('contents render correctly', () => {
        render(<ExportButton payload={mockPayload} />);
        const button = screen.getByText('Download me');
        expect(button).toBeInTheDocument();
    });

    test('user event triggered correctly once click export button', () => {
        const mockSetter = jest.fn();
        React.useState = jest.fn().mockReturnValue(["abc", mockSetter]);
        render(<ExportButton payload={mockPayload} />);
        const exportButton = screen.getByText('Export').closest('button');
        expect(exportButton).toBeInTheDocument();
        userEvent.click(exportButton as TargetElement);
        expect(mockSetter).toBeCalledTimes(1);
    });
});
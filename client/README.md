# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn run test:unit <test_case>`

run single test case, for example: yarn run test:unit "button event works correctly", will run single test case named "button event works correctly"

## Deploy

## `aws deploy push --application-name <code_deploy_application_name> --s3-location s3://<bucker_name>/<key> --ignore-hidden-files --region <region_name> --profile <local_user_profile>`

deploy to s3 bucket for CodeDeploy manually

## merge code changes into master in CodeCommit will auto trigger CodeBuild and CodeDeploy

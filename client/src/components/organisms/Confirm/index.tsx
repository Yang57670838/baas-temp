import StepperContext from "contexts/StepperContext";
import Button from "@material-ui/core/Button";
import { useDebounce } from "hooks/useDebounce";
import { useStepperContext } from "hooks/useStepperContext";
import { useFetchCronJob } from "hooks/useFetchCronJob";
import getPreferences from "store/api/preferences/getPreference";

// const searchByCurrentStep = (data: any) => {
//     return data.currentStep === 1;
// };

function Confirm() {
    const stepperState = useStepperContext();
    // const { restart, result } = useFetchCronJob(getPreferences, 4, 8, searchByCurrentStep);

    const handleNext = useDebounce((arg: any) => {
        console.log("submit, send request now..");
        // test by send request max n times in t seconds
        // restart();
    }, 1000);

    // useEffect(() => {
    //     if (result) {
    //         // found result from cronjob hook, do whatever you want below
    //         console.log("found");
    //     }
    // }, [result]);

    const handleBack = () => {
        if (stepperState) {
            stepperState.changeStep((prevActiveStep: number) => prevActiveStep - 1);
        }
    };

    return (
        <>
            {stepperState && (
                <div>
                    Confirm
                    <Button disabled={stepperState.step === 0} onClick={handleBack}>
                        Back
                    </Button>
                    <Button variant="contained" color="primary" onClick={handleNext}>
                        Submit
                    </Button>
                </div>
            )}
        </>
    );
}

export default Confirm;

import { takeLatest } from "redux-saga/effects";
import { actions as ActionTypes } from "constants/action";
import fetchToken from "./fetchToken";

export default function* watcherSaga() {
    yield takeLatest(ActionTypes.FETCH_TOKEN_REQUESTED, fetchToken);
}

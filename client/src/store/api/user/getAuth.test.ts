import getAuth from "./getAuth";

jest.mock("../index", () => {
    return {
        generateAPI: () => {
            return {
                get: () => ({
                    data: "sadsada-mocktoken",
                    status: 200
                }),
            }
        }
    }
});

describe('getAuth', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    test('should return auth token when success', async () => {
        const response = await getAuth();
        expect(response).toEqual({
            data: "sadsada-mocktoken",
            status: 200
        });
    });
  });
module.exports = {
    semi: true,
    singleQuote: true,
    trailingComma: 'all',
    arrowParens: 'avoid',
    printWidth: 120,
    bracketSpacing: true,
    bracketSameLine: true,
    tabWidth: 4
}
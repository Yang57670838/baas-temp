### `Purpose`

build a backend for frontend dashboard template with DMZ zone<br />
React & TS & Material UI as frontend, Nginx not only serves static files but also stay in the demilitarized zone, GraphQL/Nodejs as the backend but in a private trusted ec2<br />
try Redux with GraphQL client together <br />
deploy with CodeDeploy <br />
data driven form template <br />

## Build and run nginx image locally
- Build <br />
`docker build --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') -t yang/baas-temp:v1 .`
- Run locally <br />
`docker run --rm -it --name baas-temp -p 8080:80 yang/baas-temp:v1`
- Run locally in the backgroud<br />
`docker run --rm -d --name baas-temp -p 8080:80 yang/baas-temp:v1`
- Test <br />
visit 'http://localhost:8080/'

## Build and run node image locally
- Build <br />
`docker build -t yang/baas-bff-node:v1 -f ./server/Dockerfile .`
- Run locally <br />
`docker run --rm -it --name baas-bff-node -p 5000:5000 yang/baas-bff-node:v1`
- Run locally in the backgroud<br />
`docker run --rm -d --name baas-bff-node -p 5000:5000 yang/baas-bff-node:v1`
- Test <br />
visit 'http://localhost:8080/'

## unit test
- run react single unit test case  <br />
`yarn run client-test-case <test_case>`

## to do:
add CPU/MEMORY/Disk limit when launch different containers..
implement https://slickgrid.net/ grid table for bigger data
import jwtDecode from "jwt-decode";
import { UserStore } from "types/user";
import { JWTmock } from "mocks/user";

function fetchTokenSuccessHandler(state: UserStore, token: string): UserStore {
    // TO DO: when have backend, token should get from end point..
    const mockToken = JWTmock.value;
    const decodedPayload: any = jwtDecode(mockToken);

    return {
        token: mockToken,
        scope: decodedPayload.scope,
        sub: decodedPayload.sub,
        error: false,
        isLoading: false,
    };
}

export default fetchTokenSuccessHandler;

// toolbar to filter and work with summary list table
import { useState, useRef, useEffect } from "react";
import Button from "@material-ui/core/Button";
import { CSVLink } from "react-csv";
import GetAppIcon from "@material-ui/icons/GetApp";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import dayjs from "dayjs";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        hideLink: {
            display: "none",
        },
    })
);

export type OwnProps = {
    payload: Array<any>;
};

const ExportButton: React.FC<OwnProps> = ({ payload }: OwnProps) => {
    const csvLink = useRef<CSVLink & HTMLAnchorElement & { link: HTMLAnchorElement }>(null);
    const [fileName, setFileName] = useState<string>("");
    const classes = useStyles();

    useEffect(() => {
        if (fileName) {
            csvLink?.current?.link.click();
        }
    }, [fileName]);

    const downloadCSVHandler = () => {
        const now = dayjs();
        const month = now.month() < 10 ? `0${now.month()}` : now.month();
        const day = now.day() < 10 ? `0${now.day()}` : now.day();
        const hour = now.hour() < 10 ? `0${now.hour()}` : now.hour();
        const minute = now.minute() < 10 ? `0${now.minute()}` : now.minute();
        const second = now.second() < 10 ? `0${now.second()}` : now.second();
        const name = `Report-${now.year()}${month}${day}-${hour}${minute}${second}.csv`;
        setFileName(name);
    };

    return (
        <>
            {/* to do: change button font color, icon color, background color to make then look better */}
            <Button variant="contained" onClick={downloadCSVHandler} color="default" startIcon={<GetAppIcon />}>
                Export
            </Button>
            <CSVLink data={payload} filename={fileName} target="_blank" className={classes.hideLink} ref={csvLink}>
                Download me
            </CSVLink>
        </>
    );
};

export default ExportButton;

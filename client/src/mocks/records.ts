export const mockRecords: Array<any> = [
    {
        "id": "edd363a0-1e0e-439b-82b2-96a001c88382",
        "createdDate": "2021-04-19T08:23:43:851Z",
        "name": "minor service",
        "links": [
            {
                "rel": "delete",
                "href": "/record/delete"
            },
            {
                "rel": "edit",
                "href": "/record/edit"
            }
        ]
    },
    {
        "id": "8a4950fc-4b2a-49e7-b59f-7bd94cf589e5",
        "createdDate": "2021-04-19T08:23:43:851Z",
        "name": "replace tyres",
        "links": [
            {
                "rel": "delete",
                "href": "/record/delete",
                "method": "DELETE"
            },
            {
                "rel": "edit",
                "href": "/record/edit",
                "method": "POST"
            }
        ]
    }
]

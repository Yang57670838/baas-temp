import { useContext } from "react";
import StepperContext, { StepperContextInterface } from "contexts/StepperContext";

// hook to use stepper context by importing once
export const useStepperContext = () => {
    const stepperState = useContext<StepperContextInterface | null>(StepperContext);
    return stepperState;
};

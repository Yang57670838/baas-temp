import { UserStore } from "../user";
import { AppStore } from "../app";

export interface Store {
    user: UserStore;
    app: AppStore;
    router: any;
}

import { useEffect, useState, useReducer } from "react";
import { FetchDataStore } from "types/fetchData";
import { fetchDataActions } from "store/actions";
import { fetchDataReducer } from "store/reducers/fetchData";

export const initialState: FetchDataStore = {
    isLoading: false,
    error: false,
};

// make max n times requests in total t seconds, start first call after 1 second timer
// stop when find match payload result, searchCb should be defined to return true as the stopper in the middle of the t seconds
// for example: n = 3, maxSeconds = 7
export const useFetchCronJob = (cb: any, n: number, maxSeconds: number, searchCb: (payload: any) => boolean) => {
    const [fetchDataState, dispatch] = useReducer(fetchDataReducer, initialState);
    const [nextCall, setNextCall] = useState<number>(1); // at which seconds will make next call
    const [seconds, setSeconds] = useState<number>(0);
    const [isActive, setIsActive] = useState<boolean>(false);
    const [result, setResult] = useState<boolean>(false);

    const during = Math.floor((maxSeconds - 1) / (n - 1)); // after this seconds, make another call

    const restart = () => {
        if (!isActive) {
            setIsActive(true);
        }
        setSeconds(0);
        setNextCall(1);
        setResult(false);
    };

    useEffect(() => {
        let cancelRequest = false;
        const sendRequest = async () => {
            console.log("111111");
            try {
                dispatch(fetchDataActions.fetchDataRequestedAction());
                const response = await cb();
                if (!cancelRequest) {
                    console.log("calllll");
                    dispatch(fetchDataActions.fetchDataSuccessAction(response.data));
                    const check = searchCb(response.data);
                    if (!check) {
                        console.log("%%%%");
                        setNextCall(nextCall + during);
                    } else {
                        // found needed result
                        setResult(true);
                    }
                }
            } catch (error) {
                console.log("@@@@@@");
                if (!cancelRequest) {
                    dispatch(fetchDataActions.fetchDataFailedAction());
                    setNextCall(nextCall + during);
                }
            }
        };
        if (nextCall === seconds) {
            sendRequest();
        }
        return () => {
            cancelRequest = true;
        };
    }, [seconds]);

    useEffect(() => {
        let interval: any = null;
        if (isActive) {
            if (seconds > maxSeconds) {
                console.log("seconds > maxSeconds");
                setIsActive(false);
            } else {
                interval = setInterval(() => {
                    console.log("333333");
                    setSeconds((s) => s + 1);
                }, 1000);
            }
        } else if (seconds !== 0) {
            console.log("clean up");
            clearInterval(interval);
            setSeconds(0);
        }
        return () => clearInterval(interval);
    }, [isActive, seconds]);

    return { restart, result };
};

import { actions as ActionTypes } from "constants/action";
import { FetchDataStore, FetchDataActions } from "types/fetchData";

// use for useReducer in hooks only, instead of redux
export const fetchDataReducer = (state: FetchDataStore, action: FetchDataActions) => {
    switch (action.type) {
        case ActionTypes.FETCH_DATA_REQUESTED:
            return {
                ...state,
                isLoading: true,
            };
        case ActionTypes.FETCH_DATA_SUCCESS:
            return {
                isLoading: false,
                error: false,
                data: action.data,
            };
        case ActionTypes.FETCH_DATA_FAILED:
            return {
                ...state,
                isLoading: false,
                error: true,
            };
        default:
            return state;
    }
};

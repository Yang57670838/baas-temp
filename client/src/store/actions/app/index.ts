import { actions as ActionTypes } from "constants/action";
import { RequestStartAction, RequestFailedAction, RequestEndAction } from "types/app";

// to do: dispatch these actions when call request API, in order to show/hide spinner..
export const requestStartAction: () => RequestStartAction = () => {
    return {
        type: ActionTypes.REQUEST_START,
    };
};

export const requestFailedAction: (token: string) => RequestFailedAction = (token) => {
    return {
        type: ActionTypes.REQUEST_FAILED,
        token,
    };
};

export const requestEndAction: () => RequestEndAction = () => {
    return {
        type: ActionTypes.REQUEST_END,
    };
};

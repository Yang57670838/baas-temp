import { renderHook, act } from "@testing-library/react-hooks";
import { useDebounce } from "./useDebounce";

test('should trigger callback after timer pass', () => {
    jest.useFakeTimers();
    const mockCallback = jest.fn();
    const { result } = renderHook(() => useDebounce(mockCallback, 1000));
    expect(mockCallback).toBeCalledTimes(0);
    act(() => {
        result.current();
    })
    jest.advanceTimersByTime(1000);
    expect(mockCallback).toBeCalledTimes(1);
})
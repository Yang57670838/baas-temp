import express, { Application, Request, Response } from 'express';
import { readCsv } from "./utils/csvReader";
const fs = require('fs');
// const cluster = require('cluster');
const bodyParser = require('body-parser');

// check work env form image
const env = process.env.APPENV || 'dev';

const app: Application = express();
app.use(bodyParser.json());

app.get('/', (req: Request, res: Response) => res.send('API Running'));

app.get('/auth', (req: Request, res: Response) => {
    console.log('api get called', req.headers);
    res.send('mock token strings');
});

app.get('/user-preferences', (req: Request, res: Response) => {
    console.log('/user-preferences api get called');
    res.send({ currentStep: 1 });
});

app.post('/user-preferences', (req: Request, res: Response) => {
    console.log('/user-preferences api post called');
    console.log('req.body', req.body);
    fs.writeFileSync('test.json', JSON.stringify(req.body));
    res.send({ currentStep: 1 });
});

app.get('/test', (req: Request, res: Response) => {
    console.log(JSON.stringify(req.headers));
    readCsv();
    res.send('ok testsss');
});

const PORT: number = Number(process.env.PORT) || 4000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
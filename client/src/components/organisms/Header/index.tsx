import { useState } from "react";
import { ITheme } from "types/common/theme";
import { useDispatch } from "react-redux";
import { push } from "connected-react-router";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import LanguageIcon from "@material-ui/icons/Language";
import HistoryIcon from "@material-ui/icons/History";
import { makeStyles, createStyles, useTheme } from "@material-ui/core/styles";
import savePreferences from "store/api/preferences/savePreference";
import { useTranslation } from "react-i18next";

interface OwnProps {
    lngs: Array<{
        value: string;
        label: string;
    }>;
}

// jss
const style = (theme: ITheme) =>
    createStyles({
        appBar: {
            // flexGrow: 1,
        },
    } as any); // to do: ts consider string as invalid type of css property

const useStyles = makeStyles(style);

export const Header: React.FC<OwnProps> = ({ lngs }: OwnProps) => {
    const { i18n } = useTranslation();
    const dispatch = useDispatch();
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const showRecords = () => {
        dispatch(push("/user-records/2321321-21321-321321"));
    };

    const handleClose = (value: string) => {
        setAnchorEl(null);
    };

    const selectLng = (value: string) => () => {
        i18n.changeLanguage(value);
        setAnchorEl(null);
        // save lang select into user preference endpoint
        // to do: refactor this
        savePreferences();
    };

    return (
        <>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <Typography variant="h3">header</Typography>
                    <div>
                        <IconButton aria-label="languagePicker" aria-controls="menu-appbar" aria-haspopup="true" onClick={handleMenu} color="inherit">
                            <LanguageIcon />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: "top",
                                horizontal: "right",
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: "top",
                                horizontal: "right",
                            }}
                            open={open}
                            onClose={handleClose}
                        >
                            {lngs.map((lng) => {
                                return (
                                    <div key={lng.value}>
                                        <MenuItem onClick={selectLng(lng.value)}>{lng.label}</MenuItem>
                                    </div>
                                );
                            })}
                        </Menu>
                        <IconButton aria-label="languagePicker" aria-controls="menu-appbar" aria-haspopup="true" onClick={showRecords} color="inherit">
                            <HistoryIcon />
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
        </>
    );
};

export default Header;

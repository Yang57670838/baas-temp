import { UserStore, FetchTokenRequestedAction, FetchTokenSuccessAction, FetchTokenFailedAction, UndefinedUserAction } from "types/user"
import { actions as ActionTypes } from "constants/action";

export const JWTmock = {
    value: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6InVzZXI6cmVhZCIsInN1YiI6ImJmZiJ9.76196CXgsglerODBzLO_kvF6x0f0KQLLvoT9_9GoiXc',
    header: {
        'alg': 'HS256',
        'typ': 'JWT'
    },
    payload: {
        'scope': 'user:read',
        'sub': 'bff'
    }
}

export const currentState: UserStore = {
    token: "",
    scope: "",
    sub: "",
    error: false,
    isLoading: false,
};

export const mockFetchTokenRequestedAction: FetchTokenRequestedAction = {
    type: ActionTypes.FETCH_TOKEN_REQUESTED
}

export const mockFetchTokenSuccessAction: FetchTokenSuccessAction = {
    type: ActionTypes.FETCH_TOKEN_SUCCESS,
    token: JWTmock.value
}

export const mockFetchTokenFailedAction: FetchTokenFailedAction = {
    type: ActionTypes.FETCH_TOKEN_FAILED,
}

export const mockInitialAction: UndefinedUserAction = { type: ActionTypes.UNDEFINED };
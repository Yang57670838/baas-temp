import { useContext } from "react";
import { useTranslation } from "react-i18next";
import StepperContext from "contexts/StepperContext";
import Button from "@material-ui/core/Button";

function Upload() {
    const stepperState = useContext(StepperContext);
    const { t } = useTranslation("upload");

    const handleNext = () => {
        if (stepperState) {
            stepperState.changeStep((prevActiveStep: number) => prevActiveStep + 1);
        }
    };

    const handleBack = () => {
        if (stepperState) {
            stepperState.changeStep((prevActiveStep: number) => prevActiveStep - 1);
        }
    };

    return (
        <>
            {stepperState && (
                <div>
                    Upload
                    <Button disabled={stepperState.step === 0} onClick={handleBack}>
                        Back
                    </Button>
                    <Button variant="contained" color="primary" onClick={handleNext}>
                        {t("submit_button")}
                    </Button>
                </div>
            )}
        </>
    );
}

export default Upload;

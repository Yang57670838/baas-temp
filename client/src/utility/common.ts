export const onRenderCallback = (id: string, phase: string, actualDuration: number, baseDuration: number, startTime: number, commitTime: number, interactions: any) => {
    // Logic to handle the profiling details
    console.log("The component", id, ", The phase", phase, ", Time taken for the update", actualDuration, ", baseDuration: ", baseDuration, startTime, commitTime, interactions);
};

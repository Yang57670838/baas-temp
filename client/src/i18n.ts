import i18next from "i18next";
import { initReactI18next } from "react-i18next";
import HttpApi from "i18next-http-backend";
import LanguageDetector from "i18next-browser-languagedetector";

i18next
    .use(initReactI18next)
    .use(HttpApi)
    .use(LanguageDetector)
    .init({
        fallbackLng: "en",
        debug: process.env.NODE_ENV === "development",
        interpolation: {
            escapeValue: false,
        },
        backend: {
            loadPath: "/locales/{{lng}}/{{ns}}.json",
        },
    });

export const getTranslation = (ns: string, key: string, lng: string) => {
    return i18next.getResource(lng, ns, key);
};

export const getTranslationWithArg = (ns: string, key: string, arg: string, lng: string) => {
    return i18next.getFixedT(lng, ns)(key, { arg });
};

export const getCurrentLng = () => {
    return i18next.language;
};

export default i18next;

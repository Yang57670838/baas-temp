import { testSaga, expectSaga } from "redux-saga-test-plan";
import { throwError } from 'redux-saga-test-plan/providers';
import { call } from 'redux-saga/effects';
import { userActions } from "store/actions";
import getAuth from "store/api/user/getAuth";
import fetchToken from "./fetchToken";

it('works correctly when success', () => {
    testSaga(fetchToken)
        .next()
        .call(getAuth)
        .next({
            data: "mock_token"
        })
        .put(userActions.fetchTokenSuccessAction("mock_token"))
        .next()
        .isDone();
});

it('works correctly when fail', () => {
    const error = new Error('error');
    return expectSaga(fetchToken)
        .provide([
            [call(getAuth), throwError(error)],
        ])
        .put(userActions.fetchTokenFailedAction())
        .run();
});

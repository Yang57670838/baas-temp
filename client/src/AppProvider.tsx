import { ThemeProvider } from "@material-ui/core/styles";
import { Suspense } from "react";
import { ConnectedRouter, routerMiddleware } from "connected-react-router";
import { createStore, applyMiddleware, compose } from "redux";
import { createBrowserHistory } from "history";
import { Provider } from "react-redux";
import createSagaMiddleware from "redux-saga";
import { persistStore, persistReducer, PersistConfig } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";
import storage from "redux-persist/lib/storage";
import PropTypes from "prop-types";
import Spinner from "components/atoms/Spinner";
import createRootReducer from "store/reducers";
import rootSaga from "store/sagas";
import theme from "./styles/theme";

export const history = createBrowserHistory();

// TODO: split redux store part to a single file..
const persistConfig: PersistConfig<any> = {
    key: "payid",
    storage, // localstorage
    stateReconciler: autoMergeLevel2,
    whitelist: ["user"], // persist user state only here.. so refresh will keep user logged on..
};
const persistedReducer = persistReducer(persistConfig, createRootReducer(history));

const sagaMiddleware = createSagaMiddleware();

// only use redux logger in dev env
const composeEnhancers = (process.env.NODE_ENV === "development" && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;
const middlewares = [routerMiddleware(history), sagaMiddleware];
if (process.env.NODE_ENV === "development") {
    const { createLogger } = require("redux-logger"); // eslint-disable-line global-require
    const logger = createLogger({ duration: true });
    middlewares.push(logger);
}
const store = createStore(persistedReducer, composeEnhancers(applyMiddleware(...middlewares)));
const persistor = persistStore(store);
sagaMiddleware.run(rootSaga);

const AppProvider: React.FC = ({ children }) => {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <ThemeProvider theme={theme}>
                    <Suspense fallback={<Spinner />}>
                        <ConnectedRouter history={history}>{children}</ConnectedRouter>
                    </Suspense>
                </ThemeProvider>
            </PersistGate>
        </Provider>
    );
};

AppProvider.propTypes = {
    children: PropTypes.node.isRequired,
};

export default AppProvider;

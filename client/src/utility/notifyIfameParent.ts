import { NoticeType } from "types/common/iframe";
import { getParentOrigin } from "utility/getOrigin";

// tell parents that iframe app is loaded, so parent can hide the spinner
// tell parents that iframe app have error or success, so parent can show related toast messages..
export function notifyParent(type: NoticeType, content?: any) {
    const origin = process.env.REACT_APP_IFRAME_PARENT_URL || getParentOrigin();
    const win: Window = window.parent;
    win.postMessage(
        JSON.stringify({
            type,
            content,
        }),
        origin
    );
}

export function showError(errMessage: string) {
    notifyParent("error", errMessage);
}

export function appIsLoaded() {
    notifyParent("isLoaded");
}

import { Reducer } from "redux";
import { actions as ActionTypes } from "constants/action";
import { UserStore, UserActions } from "types/user";
import fetchTokenSuccessHandler from "./handler/fetchTokenSuccessHandler";

const initialState: UserStore = {
    token: "",
    scope: "",
    sub: "",
    error: false,
    isLoading: false,
};

export const userReducer: Reducer<UserStore, UserActions> = (state: UserStore = initialState, action: UserActions): UserStore => {
    switch (action.type) {
        case ActionTypes.FETCH_TOKEN_REQUESTED:
            return {
                ...state,
                isLoading: true,
                error: false,
            };
        case ActionTypes.FETCH_TOKEN_SUCCESS:
            return fetchTokenSuccessHandler(state, action.token);
        case ActionTypes.FETCH_TOKEN_FAILED:
            return {
                ...state,
                isLoading: false,
                error: true,
            };
        default:
            return state;
    }
};

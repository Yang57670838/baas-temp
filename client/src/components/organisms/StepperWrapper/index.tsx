import Stepper from "@material-ui/core/Stepper";
import { ReactElement, useEffect } from "react";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useTranslation } from "react-i18next";
import { useStepperContext } from "hooks/useStepperContext";
import { UserPreference } from "types/preferences";

interface OwnProps {
    steps: Array<string>;
    userPreference: UserPreference | undefined;
    getStepContent: (step: number) => ReactElement;
}

// to do: change max width of stepper outside div to 120rem, so in large screen, it can narrow to the middloe. like anz.
const StepperWrapper: React.FC<OwnProps> = ({ steps, getStepContent, userPreference }: OwnProps) => {
    const { t, i18n } = useTranslation();
    const stepperState = useStepperContext();

    useEffect(() => {
        if (userPreference?.currentStep) {
            stepperState?.changeStep(userPreference.currentStep);
        }
    }, [userPreference]);

    return (
        <>
            {stepperState && (
                <>
                    <Stepper activeStep={stepperState.step}>
                        {steps.map((label, index) => {
                            return (
                                <Step key={label}>
                                    <StepLabel>{t(label)}</StepLabel>
                                </Step>
                            );
                        })}
                    </Stepper>
                    <div>
                        {stepperState.step === steps.length ? (
                            <div>
                                <Typography>All steps completed - you&apos;re finished</Typography>
                            </div>
                        ) : (
                            getStepContent(stepperState.step)
                        )}
                    </div>
                </>
            )}
        </>
    );
};

export default StepperWrapper;

// toolbar to filter and work with summary list table
import { useState } from "react";
import Button from "@material-ui/core/Button";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { CSVLink } from "react-csv";
import GetAppIcon from "@material-ui/icons/GetApp";
import ExportButton from "components/molecules/ExportButton";
import styles from "./index.module.scss";

// to do: get this data from fetched grid table later.. maybe save the filterd and sorted data in redux..
const data = [
    { firstname: "Ahmed", lastname: "Tomi", email: "ah@smthing.co.com" },
    { firstname: "Raed", lastname: "Labes", email: "rl@smthing.co.com" },
    { firstname: "Yezzi", lastname: "Min l3b", email: "ymin@cocococo.com" },
];

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        button: {
            margin: theme.spacing(1),
        },
    })
);

function TableToolbar() {
    const classes = useStyles();

    return (
        <div>
            <div className={classes.button}>
                <ExportButton payload={data} />
            </div>
        </div>
    );
}

export default TableToolbar;

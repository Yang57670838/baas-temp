import { actions as ActionTypes } from "constants/action";

export interface FetchTokenRequestedAction {
    type: ActionTypes.FETCH_TOKEN_REQUESTED;
}

export interface FetchTokenSuccessAction {
    type: ActionTypes.FETCH_TOKEN_SUCCESS;
    token: string;
}

export interface FetchTokenFailedAction {
    type: ActionTypes.FETCH_TOKEN_FAILED;
}

export interface UndefinedUserAction {
    type: ActionTypes.UNDEFINED;
}

export type UserActions = FetchTokenRequestedAction | FetchTokenSuccessAction | FetchTokenFailedAction | UndefinedUserAction;

export interface UserStore {
    token: string;
    scope: string;
    sub: string;
    error: boolean;
    isLoading: boolean;
}

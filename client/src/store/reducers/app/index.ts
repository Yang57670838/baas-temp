import { Reducer } from "redux";
import { actions as ActionTypes } from "constants/action";
import { AppStore, AppActions } from "types/app";

const initialState: AppStore = {
    isRequesting: false,
};

export const appReducer: Reducer<AppStore, AppActions> = (state: AppStore = initialState, action: AppActions): AppStore => {
    switch (action.type) {
        case ActionTypes.REQUEST_START:
            return {
                ...state,
                isRequesting: true,
            };
        case ActionTypes.REQUEST_FAILED:
            return {
                ...state,
                isRequesting: false,
            };
        case ActionTypes.REQUEST_END:
            return {
                ...state,
                isRequesting: false,
            };
        default:
            return state;
    }
};

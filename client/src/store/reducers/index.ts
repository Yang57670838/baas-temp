import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { History } from "history";
import { Store } from "types/common/store";
import { userReducer } from "./user";
import { appReducer } from "./app";

const rootReducer = (history: History) =>
    combineReducers({
        router: connectRouter(history),
        user: userReducer,
        app: appReducer,
    });

// const rootReducer = (state: Store, action: any) => {
//     // if (action.type === ActionTypes.LOGOUT) {
//     //     //  persist store will also then save the init state of login reducer, which is empty data..
//     //     return appReducer(undefined, action);
//     // }
//     return appReducer(state, action);
// };

export default rootReducer;

export type AppState = ReturnType<typeof rootReducer>;

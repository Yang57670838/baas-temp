import { useSelector } from "react-redux";
import Spinner from "components/atoms/Spinner";
import { Store } from "types/common/store";
import { AppStore } from "types/app";

const SpinnerWrapper: React.FC = () => {
    const appStore: AppStore = useSelector((state: Store) => state.app);
    return <>{appStore.isRequesting && <Spinner />}</>;
};

export default SpinnerWrapper;

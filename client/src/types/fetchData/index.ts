import { actions as ActionTypes } from "constants/action";

export interface FetchDataStartedAction {
    type: ActionTypes.FETCH_DATA_REQUESTED;
}

export interface FetchDataSuccessAction {
    type: ActionTypes.FETCH_DATA_SUCCESS;
    data: any;
}

export interface FetchDataFailedAction {
    type: ActionTypes.FETCH_DATA_FAILED;
}

export interface UndefinedFetchDataAction {
    type: ActionTypes.UNDEFINED;
}

export type FetchDataActions = FetchDataStartedAction | FetchDataSuccessAction | FetchDataFailedAction | UndefinedFetchDataAction;

export interface FetchDataStore {
    data?: any;
    isLoading: boolean;
    error: boolean;
}

import { generateAPI } from "../index";

const getPreferences = () => {
    return generateAPI().get("/bff/user-preferences");
};

export default getPreferences;

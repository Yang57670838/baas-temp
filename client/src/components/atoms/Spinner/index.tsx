import styles from "./index.module.scss";

function Spinner() {
    return <div data-testid="pageSpinner" className={styles.spinner} id="pageSpinner" />;
}

export default Spinner;

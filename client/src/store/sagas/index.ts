import { fork, all } from "redux-saga/effects";
import userWatch from "./user";

export default function* rootSaga() {
    yield all([fork(userWatch)]);
}

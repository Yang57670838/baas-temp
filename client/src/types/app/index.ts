import { actions as ActionTypes } from "constants/action";

export interface RequestStartAction {
    type: ActionTypes.REQUEST_START;
}

export interface RequestFailedAction {
    type: ActionTypes.REQUEST_FAILED;
}

export interface RequestEndAction {
    type: ActionTypes.REQUEST_END;
}

export interface UndefinedRequestAction {
    type: ActionTypes.UNDEFINED;
}

export type AppActions = RequestStartAction | RequestFailedAction | RequestEndAction | UndefinedRequestAction;

export interface AppStore {
    isRequesting: boolean;
}

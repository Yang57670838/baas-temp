import { useContext } from "react";
import TimerContext, { TimerContextInterface } from "contexts/TimerContext";

export const useTimerContext = () => {
    const timerState = useContext<TimerContextInterface | null>(TimerContext);
    return timerState;
};

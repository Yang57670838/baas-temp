import { useEffect, Profiler } from "react";
import { useStepperContext } from "hooks/useStepperContext";
import { useTimerContext } from "hooks/useTimerContext";
import TimerContext from "contexts/TimerContext";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import { onRenderCallback } from "utility/common";

interface OwnProps {
    contents: Array<any>; // to do: change to strict section type later
}

const Form: React.FC<OwnProps> = ({ contents }: OwnProps) => {
    const stepperState = useStepperContext();
    const timerState = useTimerContext();

    useEffect(() => {
        timerState?.setIsActive(true);
    }, []);

    const handleNext = () => {
        if (stepperState) {
            stepperState.changeStep((prevActiveStep: number) => prevActiveStep + 1);
            // to do: check current step in StepperState, then POST call to update user preference "currentStep" in db
        }
    };

    const handleBack = () => {
        if (stepperState) {
            stepperState.changeStep((prevActiveStep: number) => prevActiveStep - 1);
        }
    };

    return (
        <Profiler id="Form" onRender={onRenderCallback}>
            {stepperState && (
                <div>
                    <Paper elevation={3} variant="outlined" square>
                        <TextField id="standard-basic" label="Standard" />
                        <TextField id="filled-basic" label="Standard" />
                        <TextField id="outlined-basic" label="Standard" />
                    </Paper>
                    <Button disabled={stepperState.step === 0} onClick={handleBack}>
                        Back
                    </Button>
                    <Button variant="contained" color="primary" onClick={handleNext}>
                        Next
                    </Button>
                    {/* to do: add dialog to save current input changes after stay 5 minutes in form */}
                    {timerState?.seconds}
                </div>
            )}
        </Profiler>
    );
};

export default Form;

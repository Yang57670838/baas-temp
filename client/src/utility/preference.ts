import momentTZ from "moment-timezone";

export const convertDatetimeToTimezone = (datetime: string, timezone: string) => {
    return momentTZ(datetime, "HH:mm").tz(timezone).format("HH:mm");
};

export const convertUTCTimeToTimezone = (UTCtime: string, timezone: string) => {
    // const d = "11:55";
    // const tz = "Australia/Melbourne";
    // return momentTZ.utc(d, "HH:mm").tz(tz).format("HH:mm");
    return momentTZ.utc(UTCtime, "HH:mm").tz(timezone).format("HH:mm");
};

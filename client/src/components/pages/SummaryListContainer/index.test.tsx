import { screen, render } from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import * as reactRedux from 'react-redux';
import TableToolbar from './index';

test('open external link correctly', () => {
    const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');
    const dummyDispatch = jest.fn()
    useDispatchMock.mockReturnValue(dummyDispatch)
    render(< TableToolbar />)
    const linkButton = screen.getByRole('button', { name: 'Learn more about My Network Solution' });
    expect(linkButton).toBeInTheDocument();
    global.open = jest.fn();
    userEvent.click(linkButton);
    expect(global.open).toBeCalled();
});
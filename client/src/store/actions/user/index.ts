import { actions as ActionTypes } from "constants/action";
import { FetchTokenRequestedAction, FetchTokenSuccessAction, FetchTokenFailedAction } from "types/user";

export const fetchTokenRequestedAction: () => FetchTokenRequestedAction = () => {
    return {
        type: ActionTypes.FETCH_TOKEN_REQUESTED,
    };
};

export const fetchTokenSuccessAction: (token: string) => FetchTokenSuccessAction = (token) => {
    return {
        type: ActionTypes.FETCH_TOKEN_SUCCESS,
        token,
    };
};

export const fetchTokenFailedAction: () => FetchTokenFailedAction = () => {
    return {
        type: ActionTypes.FETCH_TOKEN_FAILED,
    };
};

# to do: move this file into client directory..
FROM node:10-alpine AS staticbuilder
WORKDIR /app
COPY . ./
RUN yarn install && yarn run client-build


FROM nginx:alpine

ARG BUILD_DATE

LABEL maintainer="yangliu1010my@gmail.com"
LABEL author="yang"
LABEL version="beta1.0"
LABEL name="bff-template"
LABEL description="a baas structure app template"
LABEL build_date=$BUILD_DATE

COPY client/nginx.conf /etc/nginx/nginx.conf
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=staticbuilder /app/client/build .
RUN chmod -R 777 /usr/share/nginx/html/ /etc/nginx
ENTRYPOINT ["nginx", "-g", "daemon off;"]
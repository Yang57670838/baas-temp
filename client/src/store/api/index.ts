import axios, { AxiosError, AxiosInstance } from "axios";

export const generateAPI = (baseUrl = "/"): AxiosInstance => {
    const api = axios.create({
        baseURL: baseUrl,
    });

    api.interceptors.request.use(
        (config) => {
            return {
                ...config,
                headers: {
                    Authorization: "Bearer 23213213",
                    "X-Custom-Header": "value",
                },
            };
        },
        (error) => {
            Promise.reject(error);
        }
    );

    api.interceptors.response.use(
        (response) => {
            return response;
        },
        (error: AxiosError) => {
            const responseCode = error?.response?.status;
            switch (responseCode) {
                default:
                    console.log("general error, pop up toast");
                // to do: call store action to pop up a general toast error message in screen..
            }
            return Promise.reject(error);
        }
    );
    return api;
};

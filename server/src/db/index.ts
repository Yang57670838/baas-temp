import cls from 'cls-hooked';
import { Sequelize } from 'sequelize';

export default class Database {
    environment: string;
    dbConfig: any;
    isTestEnvironment: boolean;

    constructor(environment: string, dbConfig: any) {
        this.environment = environment;
        this.dbConfig = dbConfig;
        this.isTestEnvironment = this.environment === 'test';
    }

    async connect() {
        // setup the namespace for transactions
        const namespace = cls.createNamespace('transaction-namespace');

    }

    async disconnect() {

    }

    async sync() {

    }
}

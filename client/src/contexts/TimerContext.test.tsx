import { screen, render, act } from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import { useContext } from "react";
import TimerContext, { TimerStore } from './TimerContext';

function Mock() {
    const timerState = useContext(TimerContext);

    const handle = () => {
        timerState?.restart(5);
        timerState?.setSeconds(2);
    };

    const stopTimer = () => {
        timerState?.setIsActive(false);
    }

    return (
        <>
            <span>{timerState?.seconds}</span>
            <span>{timerState?.isActive ? "true" : "false"}</span>
            <button onClick={handle}>
                Click
            </button>
            <button onClick={stopTimer}>
                Stop
            </button>
        </>
    );
}

describe('Timer context', () => {
    beforeEach(() => {
        render(
            <TimerStore>
                <Mock />
            </TimerStore>
        );
    });

    test('init correctly', () => {
        expect(screen.getByText("0")).toBeInTheDocument();
        expect(screen.getByText("false")).toBeInTheDocument();
    });

    test('start timer correctly', () => {
        jest.useFakeTimers();
        expect(screen.getByText("0")).toBeInTheDocument();
        expect(screen.getByText("false")).toBeInTheDocument();

        const button = screen.getByRole("button", { name: "Click"});
        userEvent.click(button);
        expect(screen.getByText("true")).toBeInTheDocument();
        expect(screen.getByText("2")).toBeInTheDocument();

        userEvent.click(button);

        act(() => jest.advanceTimersByTime(1000) as any);
        expect(screen.getByText("3")).toBeInTheDocument();

        const stopButton = screen.getByRole("button", { name: "Stop"});
        userEvent.click(stopButton);
        expect(screen.getByText("false")).toBeInTheDocument();

        userEvent.click(button);
    });
});
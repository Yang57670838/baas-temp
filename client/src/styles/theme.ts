import { createTheme } from "@material-ui/core/styles";
import { IThemeOptions } from "types/common/theme";

const primaryBlue = "#0E4165";
const secondaryBlue = "#DFC5A9";
const primaryRed = "#bd1600";

export default createTheme({
    palette: {
        primary: {
            main: `${primaryBlue}`,
        },
        secondary: {
            main: `${secondaryBlue}`,
        },
    },
    typography: {
        tab: {
            textTransform: "none",
            fontweight: 700,
            fontSize: "1rem",
        },
        headerButton: {
            fontSize: "1rem",
            textTransform: "none",
            color: "white",
        },
    },
} as any);

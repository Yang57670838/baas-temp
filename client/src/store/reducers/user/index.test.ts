import { userReducer } from "./index";
import { currentState, mockInitialAction, mockFetchTokenRequestedAction, mockFetchTokenSuccessAction, mockFetchTokenFailedAction, JWTmock } from "mocks/user";

describe('user reducer', () => {
	test('should return the initial state', () => {
        expect(userReducer(undefined, mockInitialAction)).toEqual(currentState);
    });

    test('should handler FETCH_TOKEN_REQUESTED action', () => {
        expect(userReducer(currentState, mockFetchTokenRequestedAction)).toEqual({
            token: "",
            scope: "",
            sub: "",
            error: false,
            isLoading: true,
        });
    });

    test('should handler FETCH_TOKEN_SUCCESS action', () => {
        expect(userReducer({ token: "", scope: "", sub: "", error: false, isLoading: true }, mockFetchTokenSuccessAction)).toEqual({
            token: JWTmock.value,
            scope: JWTmock.payload.scope,
            sub: JWTmock.payload.sub,
            error: false,
            isLoading: false,
        });
    });

    test('should handler FETCH_TOKEN_FAILED action', () => {
        expect(userReducer({ token: "", scope: "", sub: "", error: false, isLoading: true }, mockFetchTokenFailedAction)).toEqual({
            token: "",
            scope: "",
            sub: "",
            error: true,
            isLoading: false,
        });
    });
});
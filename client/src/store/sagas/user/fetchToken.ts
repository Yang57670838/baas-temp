import { call, put } from "redux-saga/effects";
import { userActions } from "store/actions";
import { ApiResponse } from "types/common/global";
import getAuth from "store/api/user/getAuth";

export interface FetchTokenResponse extends ApiResponse {
    data: string;
}

export default function* fetchToken() {
    try {
        const payload: FetchTokenResponse = yield call(getAuth);
        console.log("getAuth api res payload", payload);
        yield put(userActions.fetchTokenSuccessAction(payload.data));
    } catch (e) {
        yield put(userActions.fetchTokenFailedAction());
    }
}

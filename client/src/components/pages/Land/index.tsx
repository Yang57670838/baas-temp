import Button from "@material-ui/core/Button";
import { useDispatch } from "react-redux";
import { push } from "connected-react-router";

import("../Contactus");

function Land() {
    const dispatch = useDispatch();

    const land = () => {
        dispatch(push("/summary-list"));
    };

    return (
        <>
            <Button variant="contained" color="primary" onClick={land}>
                Next
            </Button>
        </>
    );
}

export default Land;

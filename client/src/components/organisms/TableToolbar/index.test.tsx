import { render } from '@testing-library/react';
import TableToolbar from './index';

test('renders TableToolbar component', () => {
  const { getByText } = render(<TableToolbar />);
  const linkElement = getByText(/Export/i);
  expect(linkElement).toBeInTheDocument();
});

import { generateAPI } from "../index";

// move this to iframe app.. only iframe app needs save user preferece for filter and search behaviours..
const savePreferences = () => {
    return generateAPI().post("/bff/user-preferences", {
        id: "test-pref",
        data: "test",
    });
};

export default savePreferences;

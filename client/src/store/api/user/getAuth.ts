import { generateAPI } from "../index";

const getAuth = () => {
    return generateAPI().get("/bff/auth");
};

export default getAuth;

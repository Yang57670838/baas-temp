import { actions as ActionTypes } from "constants/action";
import { FetchDataStartedAction, FetchDataSuccessAction, FetchDataFailedAction } from "types/fetchData";

export const fetchDataRequestedAction: () => FetchDataStartedAction = () => {
    return {
        type: ActionTypes.FETCH_DATA_REQUESTED,
    };
};

export const fetchDataSuccessAction: (data: any) => FetchDataSuccessAction = (data) => {
    return {
        type: ActionTypes.FETCH_DATA_SUCCESS,
        data,
    };
};

export const fetchDataFailedAction: () => FetchDataFailedAction = () => {
    return {
        type: ActionTypes.FETCH_DATA_FAILED,
    };
};

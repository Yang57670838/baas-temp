import { screen, render } from '@testing-library/react';
import userEvent, { TargetElement } from "@testing-library/user-event";
import React from "react";
import renderer from "react-test-renderer";
import StepperContext from "contexts/StepperContext";
import Form from './index';

describe('Form component snapshot', () => {
    test('renders correctly', () => {
        const tree = renderer.create(<Form contents={[]} />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});

describe('Form component', () => {
    test('button event works correctly', () => {
        const mockFunc = jest.fn();
        render(
            <StepperContext.Provider value={{ step: 0, changeStep: mockFunc }}>
                <Form contents={[]} />
            </StepperContext.Provider>
        );
        const button = screen.getByText('Next').closest('button');
        expect(button).toBeInTheDocument();
        expect(mockFunc).toBeCalledTimes(0);
        userEvent.click(button as TargetElement);
        expect(mockFunc).toBeCalledTimes(1);
    });
});
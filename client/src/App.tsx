import { lazy, useEffect } from "react";
import { Route, Switch } from "react-router-dom";
import queryString from "query-string";
import { userActions } from "store/actions";
import { Store } from "types/common/store";
import { useDispatch, useSelector } from "react-redux";
import SpinnerWrapper from "components/organisms/SpinnerWrapper";
import { appIsLoaded } from "utility/notifyIfameParent";
import { convertDatetimeToTimezone } from "utility/preference";
import styles from "./App.module.scss";

const SummaryListContainer = lazy(() => import(/* webpackChunkName: "SummaryList" */ "components/pages/SummaryListContainer"));
const ContentManagement = lazy(() => import(/* webpackChunkName: "ContentManagement" */ "components/pages/ContentManagement"));
const UserRecords = lazy(() => import(/* webpackChunkName: "UserRecords" */ "components/pages/UserRecords"));
const Land = lazy(() => import(/* webpackChunkName: "Land" */ "components/pages/Land"));

// to do: change to connected react router later
function App() {
    const dispatch = useDispatch();
    const { location } = useSelector((reduxstore: Store) => reduxstore.router);

    useEffect(() => {
        // load form based on app id
        // to do check if inside iframe or not
        const { appid } = queryString.parse(location.search);
        console.log("appssssss", appid);
        // to do: when get app id, instead of ask for login, directly show form page..
        dispatch(userActions.fetchTokenRequestedAction());
        // tell iframe parent this app is loaded
        appIsLoaded();
        const test = convertDatetimeToTimezone("aa", "ww");
        console.log("test", test);
    }, []);

    return (
        <>
            <SpinnerWrapper />
            <Switch>
                <Route exact path="/summary-list" component={SummaryListContainer} />
                <Route exact path="/dashboard" component={ContentManagement} />
                <Route exact path="/user-records/:id" component={UserRecords} />
                <Route component={Land} />
            </Switch>
        </>
    );
}

export default App;

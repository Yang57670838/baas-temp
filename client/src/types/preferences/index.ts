import { actions as ActionTypes } from "constants/action";

export interface UserPreference {
    currentStep: number;
}

export interface FetchUserPreferencesStartedAction {
    type: ActionTypes.FETCH_USER_PREFERENCES_STARTED;
}

export interface FetchUserPreferencesSuccessAction {
    type: ActionTypes.FETCH_USER_PREFERENCES_SUCCESS;
    preferences: UserPreference;
}

export interface FetchUserPreferencesFailedAction {
    type: ActionTypes.FETCH_USER_PREFERENCES_FAILED;
}

export interface UndefinedUserPreferenceAction {
    type: ActionTypes.UNDEFINED;
}

export type UserPreferencesActions = FetchUserPreferencesStartedAction | FetchUserPreferencesSuccessAction | FetchUserPreferencesFailedAction | UndefinedUserPreferenceAction;

export interface PreferenceStore {
    preferences?: UserPreference;
    isLoading: boolean;
    error: boolean;
}

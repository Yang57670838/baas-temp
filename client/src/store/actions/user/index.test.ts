import * as userActions from "./index";
import { mockFetchTokenRequestedAction, mockFetchTokenSuccessAction, mockFetchTokenFailedAction, JWTmock } from "mocks/user";

describe('user store actions', () => {
    test('if fetchTokenRequestedAction creator works', () => {
        expect(userActions.fetchTokenRequestedAction()).toEqual(mockFetchTokenRequestedAction)
    });

    test('if fetchTokenSuccessAction creator works', () => {
        expect(userActions.fetchTokenSuccessAction(JWTmock.value)).toEqual(mockFetchTokenSuccessAction)
    });

    test('if fetchTokenFailedAction creator works', () => {
        expect(userActions.fetchTokenFailedAction()).toEqual(mockFetchTokenFailedAction)
    });
});

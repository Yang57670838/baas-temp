import { useState, useEffect, useReducer } from "react";
import getPreferences from "store/api/preferences/getPreference";
import { actions as ActionTypes } from "constants/action";
import { PreferenceStore, UserPreferencesActions } from "types/preferences";

export const initialState: PreferenceStore = {
    isLoading: false,
    error: false,
};
// to do: switch to useFetch hook..
export const preferenceReducer = (state: PreferenceStore, action: UserPreferencesActions) => {
    switch (action.type) {
        case ActionTypes.FETCH_USER_PREFERENCES_STARTED:
            return {
                ...state,
                isLoading: true,
            };
        case ActionTypes.FETCH_USER_PREFERENCES_SUCCESS:
            return {
                error: false,
                isLoading: false,
                preferences: action.preferences,
            };
        case ActionTypes.FETCH_USER_PREFERENCES_FAILED:
            return {
                ...state,
                isLoading: false,
                error: true,
            };
        default:
            return state;
    }
};

// create a fetch hook for user preferences, but to save user preferences, it will happen when user click save button and save form contents with user prefer concurrently..
const useFetchUserPrefer = () => {
    const [state, dispatch] = useReducer(preferenceReducer, initialState);

    useEffect(() => {
        let cancelRequest = false;
        const fetchData = async () => {
            try {
                dispatch({ type: ActionTypes.FETCH_USER_PREFERENCES_STARTED });
                const response = await getPreferences();
                const result = response.data;
                if (!cancelRequest) {
                    dispatch({ type: ActionTypes.FETCH_USER_PREFERENCES_SUCCESS, preferences: result });
                }
            } catch (error) {
                if (!cancelRequest) {
                    dispatch({ type: ActionTypes.FETCH_USER_PREFERENCES_FAILED });
                }
            }
        };
        fetchData();
        return function cleanup() {
            cancelRequest = true;
        };
    }, []);

    return { state };
};

export default useFetchUserPrefer;

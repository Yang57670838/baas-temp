import { screen, render } from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import { useContext } from "react";
import StepperContext, { StepperStore } from './StepperContext';

function Mock() {
    const stepperState = useContext(StepperContext);

    const handleNext = () => {
        if (stepperState) {
            stepperState.changeStep((prevActiveStep: number) => prevActiveStep + 1);
        }
    };

    return (
        <>
            {stepperState && (
                <button onClick={handleNext} data-step={stepperState.step}>
                    Click
                </button>
            )}
        </>
    );
}

describe('Stepper context', () => {
    beforeEach(() => {
        render(
            <StepperStore>
                <Mock />
            </StepperStore>
        );
    });

    test('run correctly', () => {
        const buttonElement = screen.getByRole('button', { name: "Click" });
        expect(buttonElement).toBeInTheDocument();
        expect(buttonElement).toHaveAttribute('data-step', '0');

        userEvent.click(buttonElement);
        expect(buttonElement).toHaveAttribute('data-step', '1');
    });
});
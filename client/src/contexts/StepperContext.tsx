import { createContext, useState, ReactNode } from "react";

export interface StepperContextInterface {
    step: number;
    changeStep: (argument: any) => void;
}

interface OwnProps {
    children: ReactNode;
}

const Context = createContext<StepperContextInterface | null>(null);

export const StepperStore: React.FC<OwnProps> = ({ children }: OwnProps) => {
    const [step, changeStep] = useState<number>(0);

    return <Context.Provider value={{ step, changeStep }}>{children}</Context.Provider>;
};

export default Context;

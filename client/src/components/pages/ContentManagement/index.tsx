// move it out of baas
import { useState, MouseEvent } from "react";
import Table from "@material-ui/core/Table";
import { makeStyles } from "@material-ui/core/styles";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { mockRecords } from "mocks/records";
import TableToolbar from "components/organisms/TableToolbar";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export interface PointerCoordinate {
    mouseX: number | null;
    mouseY: number | null;
}

export const initialPointerCoordinate: PointerCoordinate = {
    mouseX: null,
    mouseY: null,
};

function ContentManagement() {
    const [pointerCoordinate, setPointerCoordinate] = useState<PointerCoordinate>(initialPointerCoordinate);
    const classes = useStyles();

    const handleClick = (event: MouseEvent<HTMLElement>) => {
        event.preventDefault();
        setPointerCoordinate({
            mouseX: event.clientX - 2,
            mouseY: event.clientY - 4,
        });
    };

    const handleClose = () => {
        setPointerCoordinate(initialPointerCoordinate);
    };

    return (
        <>
            <TableContainer component={Paper}>
                <TableToolbar />
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell align="right">Date</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {mockRecords.map((row) => (
                            <TableRow key={row.name} onContextMenu={handleClick}>
                                <TableCell component="th" scope="row">
                                    {row.name}
                                </TableCell>
                                <TableCell align="right">{row.createdDate}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Menu
                keepMounted
                open={pointerCoordinate.mouseY !== null}
                onClose={handleClose}
                anchorReference="anchorPosition"
                anchorPosition={
                    pointerCoordinate.mouseY !== null && pointerCoordinate.mouseX !== null ? { top: pointerCoordinate.mouseY, left: pointerCoordinate.mouseX } : undefined
                }
            >
                {/* to do: add HATEOAS url into each action in the menu and top action bar.. */}
                <MenuItem onClick={handleClose}>Edit</MenuItem>
                <MenuItem onClick={handleClose}>Delete</MenuItem>
            </Menu>
        </>
    );
}

export default ContentManagement;

import { createContext, useState, ReactNode, useEffect } from "react";

export interface TimerContextInterface {
    seconds: number;
    setIsActive: (argument: any) => void;
    setSeconds: (argument: any) => void;
    restart: (max: number) => void;
    isActive: boolean;
}

interface OwnProps {
    children: ReactNode;
}

const Context = createContext<TimerContextInterface | null>(null);

export const TimerStore: React.FC<OwnProps> = ({ children }: OwnProps) => {
    const [seconds, setSeconds] = useState<number>(0);
    const [maxSeconds, setMaxSeconds] = useState<number>(0);
    const [isActive, setIsActive] = useState<boolean>(false);

    const restart = (max: number) => {
        if (!isActive) {
            setIsActive(true);
        }
        setSeconds(0);
        setMaxSeconds(max);
    };

    useEffect(() => {
        let interval: any = null;
        if (isActive) {
            if (seconds >= maxSeconds) {
                setIsActive(false);
            } else {
                interval = setInterval(() => {
                    setSeconds((s) => s + 1);
                }, 1000);
            }
        } else if (!isActive && seconds !== 0) {
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [isActive, seconds]);

    return <Context.Provider value={{ seconds, setIsActive, setSeconds, isActive, restart }}>{children}</Context.Provider>;
};

export default Context;

// list of search results in a table, with buttons on the top to work with data
import { ReactElement } from "react";
import { Button } from "@material-ui/core";
import { ITheme } from "types/common/theme";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import StepperWrapper from "components/organisms/StepperWrapper";
import Upload from "components/organisms/Upload";
import Confirm from "components/organisms/Confirm";
import Form from "components/organisms/Form";
import { StepperStore } from "contexts/StepperContext";
import { TimerStore } from "contexts/TimerContext";
import Header from "components/organisms/Header";
import useFetchUserPrefer from "hooks/useFetchUserPrefer";
import logo from "./mockLogo.jpeg";

// jss
const style = (theme: ITheme) =>
    createStyles({
        footer: {
            position: "fixed",
            bottom: "3rem",
        },
        logoImage: {
            marginRight: "1rem",
            width: "110px",
            height: "55px",
            display: "inline-block",
            verticalAlign: "middle",
            backgroundImage: `url(${logo})`,
        },
    } as any); // to do: ts consider string as invalid type of css property

const useStyles = makeStyles(style);

// to do: move step contents below from endpoint JSON result
const mockFormPayload = {
    steps: ["label_form", "label_upload", "label_confirm"],
    stepContents: [
        {
            type: "form",
            contents: [
                {
                    type: "input",
                },
                {
                    type: "input",
                },
            ],
        },
        {
            type: "upload",
            contents: [],
        },
        {
            type: "confirm",
            contents: [],
        },
    ],
    lng: [
        {
            value: "zh",
            label: "中文",
        },
        {
            value: "en",
            label: "English",
        },
    ],
};

const getStepContent = (stepIndex: number): ReactElement => {
    const contentPayload = mockFormPayload.stepContents[stepIndex];
    switch (contentPayload.type) {
        case "form":
            return <Form contents={contentPayload.contents} />;
        case "upload":
            return <Upload />;
        case "confirm":
            return <Confirm />;
        default:
            return <div>no matching component</div>;
    }
};

function SummaryListContainer() {
    const classes = useStyles();
    const { state } = useFetchUserPrefer();

    const openLink = () => {
        window.open("https://myns.com.au");
    };

    return (
        <>
            <Header lngs={mockFormPayload.lng} />
            <h1>management</h1>
            <br />
            <br />
            <br />
            <br />
            <TimerStore>
                <StepperStore>
                    <StepperWrapper getStepContent={getStepContent} steps={mockFormPayload.steps} userPreference={state.preferences} />
                </StepperStore>
            </TimerStore>

            <div className={classes.footer}>
                {/* to do: get a logo match this size */}
                <div className={classes.logoImage} />
                <Button variant="outlined" color="primary" onClick={openLink}>
                    Learn more about My Network Solution
                </Button>
            </div>
        </>
    );
}

export default SummaryListContainer;

import jwt from 'jsonwebtoken';
import JWTUtiles from '../../src/utils/jwt';
import jwtUtils from '../../src/utils/jwt';

describe('jwt utils', () => {
    it('should generate access token', () => {
        const payload = { email: 'test@gmail.com' };
        expect(jwtUtils.generateAccessToken(payload)).toEqual(expect.any(String));
    })

    it('should verify that the access token is valid', () => {
        const payload = { email: 'test@gmail.com' };
        const jwt = jwtUtils.generateAccessToken(payload);
        expect(jwtUtils.verifyAccessToken(jwt)).toEqual(
            expect.objectContaining(payload)
        );
    })

    it('should error iff access token is invalid', () => {
        expect(() => JWTUtiles.verifyAccessToken('invalid.mock.token')).toThrow(jwt.JsonWebTokenError);
    })
})

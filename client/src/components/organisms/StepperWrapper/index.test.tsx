import { screen, render } from '@testing-library/react';
import renderer from "react-test-renderer";
import StepperWrapper from './index';
import StepperContext from "contexts/StepperContext"

jest.mock('react-i18next', () => ({
    useTranslation: () => {
      return {
        t: (str: string) => str,
        i18n: {
          changeLanguage: () => new Promise(() => {}),
        },
      };
    },
  }));

describe('StepperWrapper component', () => {
    test('renders with correct contents', () => {
        render(
            <StepperContext.Provider value={{ step: 3, changeStep: () => null}}>
                <StepperWrapper userPreference={undefined} steps={["label_form", "label_upload", "label_confirm"]} getStepContent={jest.fn()} />
            </StepperContext.Provider>
        )
        const label1 = screen.getByText('label_form');
        expect(label1).toBeInTheDocument();
    });
});